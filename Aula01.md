---
marp: true
---

# Aula 01 - Introdução à Disciplina

___

## Apresentação do Professor
### Ramon Venson
* Bacharel em Ciência da Computação (UNESC)
* Especialização em Tecnologias Web
* Mestrado em Tecnologia da Informação e Comunicação
* Professor e desenvolvedor

___

#### Interesses

* Desenvolvimento Web
    * Node.JS
    * Firebase
    * React.JS
    * HTML & CSS
    * PHP

___

* Desenvolvimento de Jogos
    * Unity 3D
    * Godot
    * Blender

* Administração de Servidores
    * GNU/Linux
    * Shell Script
    * Cloud Computing

___

## Apresentação da Disciplina

### Ementa

Estudo e implementação de WebServices com Rest API's. Modelos de comunicação síncrona e assíncrona em frameworks  e desenvolvimento web. Comunicação com banco de dados  relacional utilizando ORM e com banco OO. Projeto de desenvolvimento de software integrando as tecnologias estudadas.

___

### Objetivo Geral

Identificar e empregar tecnologias e padrões de desenvolvimento na construção de ferramentas direcionadas para a Web.

___

### Objetivos Específicos

* Identificar as principais tecnologias direcionadas para a construção de aplicações web;
* Apresentar modelos de organização, controle e gerenciamento de projetos de software;
* Analisar interação entre tecnologias de desenvolvimento de software na web;
* Desenvolver a autonomia na escolha e utilização de tecnologias web.

___

### Conteúdo Programático

1. Introdução à Disciplina e Ambiente
1. Linguagem JavaScript (ES6)
1. Controle de Versão (GIT)
1. Gerenciamento de Projetos (Gitlab)
1. Aplicações com Node.JS
1. Modelo de Troca de Dados JSON
1. Aplicações RESTFul
1. Persistência de Dados (MongoDB)
1. Estilização (CSS e Bootstrap)
1. React.JS

___

### Calendário

* 27/02 - Aula 01: Introdução à Disciplina
* 05/03 - Aula 02: Javascript (Básico)
* 12/03 - Aula 03: Javascript ES6
* 19/03 - Aula 04: Versionamento com Git
* 26/03 - Aula 05: Gitlab e Controle de Projetos
* 02/04 - Aula 06: Introdução ao Node.JS
* 09/04 - Recesso
* 16/04 - Aula 07: Web Server com Node.JS
* 23/04 - Aula 08: Comunicação de Dados com JSON

___

* 30/04 - Aula 09: REST e Protocolo HTTP
* 07/05 - Aula 10: Persistência com MongoDB e Mongoose
* 14/05 - Aula 11: Projeto Intermediário
* 21/05 - Aula 12: Estilização (CSS e Bootstrap)
* 28/05 - Aula 13: Frontend com React.JS (Básico)
* 04/06 - Aula 14: Comunicação Frontend/Backend
* 11/06 - Recesso
* 18/06 - Aula 15: Desenvolvimento do Projeto Final
* 25/06 - Aula 16: Apresentação Projeto Final
* 02/07 - Aula 17: Bônus: GraphQL, Firebase, React Native, Electron...
* 09/07 - Aula 18: Bônus: GraphQL, Firebase, React Native, Electron...

___

### Metodologia

### Nota

* N1: Exercícios e pesquisas
* N2: Projeto Intermediário
* N3: Projeto Final
* **Média Final = (N1 + N2 + N3) / 3**

___

#### Outras considerações
* Atividades em sala de aula deverão ser apresentadas ao final da aula ao professor, ou de acordo com o cronograma definido.
* Todos os exercícios e projetos devem ser postados no ambiente virtual ou mantidos em repositório remoto, de acordo com instruções do professor.
* Os projetos serão apresentados à turma, ou diretamente ao professor caso assim definido.
* A avaliação de trabalhos será feita de forma individualizada.

___

## Conceitos Básicos

___

### Conceitos de um Fullstack Webdeveloper

## Santíssima Trindade

![](../../Assets/img/html_css_js.png)

* HTML
* CSS
* Javascript

[[Mozilla] Introdução ao desenvolvimento web](https://developer.mozilla.org/en-US/docs/Web/Guide/Introduction_to_Web_development)

___

## Outros Conceitos

* Lógica de Programação
* Estrutura de Dados
* Linguagem de Programação
* Design Patterns
* Performance
* Terminal e Console

___

* Internet (protocolos, dados, requisições...)
* Protocolo HTTP/HTTPS
* Document Object Model (DOM)
* Cascading Style Sheets (CSS)
* Design Responsivo
* Interface e Experiência de Usuário (UX e UI)
* Web Browsers (DevTools e Funcionalidades)
* Search Engine Optimization (SEO)
* DevOps
* Localização e Acessibilidade

___

* Controle de Versão
* Testes
* Gerenciamento de Projetos
* Ciclo de Vida (Engenharia de Software)
* Banco de Dados
* Frameworks e Ambientes
* Application Programming Interfaces (API)
* Segurança da Informação
* Línguas (Inglês e Espanhol)
* Toneladas de bibliotecas e ferramentas

___

## O que vamos usar?

* HTML
* CSS
* Javascript (ES6)
* Git
* Gitlab
* JSON
* MongoDB
* Bootstrap
* Node.JS
* React

___

## Ferramentas

* ``atom`` ou ``visual studio code``
* Navegador (``firefox`` ou ``chrome``)
* ``git``
* Gerenciador de Pacotes ``npm``
* ``mongodb``
* Editores de Imagem

___

## HTML (HyperText Markup Language)

````html
<h1>Olá mundo</h1>
Bem-vindo à disciplina, <b>estudante</b>!
````

* Linguagem de Marcação
* Estrutura os dados no frontend
* Interpretado pelos navegadores web

___

## CSS (Cascading Style Sheets)
````css
body {
    background-color: #d8da3d;
}
.produtos {
    margin: 10px;
    color: #tomato;
}
````

* Estiliza o conteúdo de um documento HTML na renderização do navegador
* Permite compartilhar estilos entre diferentes documentos
* Habilita o design responsivo

___

## JavaScript

````javascript
document.getElementById('linha_produto').onclick = function() {
    alert('Criando produto!');
    var produto = document.createTextNode('Smarthphone');
    document.body.appendChild(produto);
}
````

* Linguagem interpretada de alto nível
* Multi-paradigma (dynamic typing, prototype-based, object-orientation)
* Atualmente, possui usos em diversos ambientes
* Sintaxe ES6

___

## Git

````git
git init
git add .
git commit -m "Meu primeiro commit"
git push
````

* Ferramenta de versionamento de código
* Controla alterações e mantém histórico
* Garante integridade em múltiplas operações

___

## Gitlab

![](../../Assets/img/Gitlab_logo.png)

* Gerenciador de Repositórios Git
* Organiza equipes e planejamento
* Ferramentas para documentação e desenvolvimento ágil
* Open Source

___

## JSON (JavaScript Object Notation)

````json
{
    "equipe": {
        "criciuma" : {
            "vitorias": 12,
            "empates": 2,
            "derrotas": 0
        }
    }
}
````

* Derivado do Javascript
* Formato de representação de dados
* Popularizado por webservices

___

## MongoDB

````javascript
db.users.insert({username:"google",password:"google123"})
db.users.find()

> { "_id" : ObjectId("504f48ea17f6c778042c3c0a"), "username" : "google", "password" : "google123" }
````

* Orientado à documentos
* NoSQL
* Semelhante ao JSON

___

## Bootstrap

![](../../Assets/img/bootstrap_exemple.png)

````html
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
````

* Framework front-end
* Projeto de open source do twitter
* Estilização pré-constituida

> Ver também o [Zurb Foundation](https://foundation.zurb.com/)

___

## Node.JS

````javascript
var express = require('express');
var app = express()

app.get('/', function (req, res) {
  res.send('Olá Mundo!')
})

app.listen(3000)
````

* Interpretador JavaScript
* Multiplataforma
* Server-side

___

## React

````javascript
class BoasVindas extends React.Component {
    render() {
      return <h1>Bem-vindo, {this.props.usuario}</h1>
    }
}

ReactDOM.render(<BoasVindas usuario="rvenson" />, document.getElementById('paginaPrincipal'));
````

* Biblioteca JavaScript para interfaces de usuário
* Estados e propriedades
* Ciclo de vida
* Linguagem JSX

___

## Comunidades e Links

* Mozilla ([link](https://mozilla.org))
* W3Schools ([link](https://www.w3schools.com))

___

## Exercícios

1. Crie uma página em HTML que contenha uma lista contendo jogos, filmes ou séries do seu gosto. Esta lista deve conter:
    * Uma sinopse;
    * Uma seção com imagens;
    * Uma lista contendo nome e descrição dos personagens;
    * Um link para uma página externa (ex.: Wikipedia);
    * Outras informações também são bem-vindas.

``Não esqueça de empacotar a página e enviar ao professor``
___

2. Crie uma página chamada ``criacao_personagem.html`` que contenha um formulário para criação de um personagem, contendo os seguintes dados:
    * Nome do Personagem [nome]
    * Cidade Natal (Thais, Carlin, Venore) [cidade]
    * Classe (Guerreiro, Arqueiro, Mago) [classe]
    * Raça (Humano, Elfo, Anão) [raca]
    * Realizar Tutorial? (Boolean) [tutorial]

    O formulário deve ser do tipo GET e enviar os dados para ``https://venson.net.br/professor/ws/personagem``

``Não esqueça de empacotar a página e enviar ao professor``

___

3. Estilize o formulário do exercício anterior com CSS para que fique como o [mockup](exercicio03_mockup.jpg). O CSS deve seguir as seguintes especificações:
    * O fundo da página deve ser da cor ``#8083c9``e a fonte deve ``monospace``
    * A cor da fonte deve ser ``white``
    * O formulário deve estar posicionado horizontalmente ao centro da página, com tamanho máximo de 20rem;
    * A cor de fundo do formulário deve ser branca, com bordas sólidas na cor `slateblue` de 2px;
    * Os títulos dos campos devem estar na cor rgb(38, 28, 105), em maiúscula
    * Os inputs de texto devem ter uma borda abaixo de 1px na cor `lightgray`. Também devem ocupar 100% do espaço disponível no formulário
    * O botão de registro deve ser da cor ``slateblue``, com a fonte branca

``Não esqueça de empacotar a página e enviar ao professor``
___

4. Crie um script na linguagem javascript para o envio do personagem. Os dados do formulário só devem ser enviados ao servidor caso sigam as seguintes regras:
    * O nome do personagem deve conter pelo menos 3 letras
    * O personagem não pode iniciar com as palavras:
        * Admin
        * GM
        * Moderador
    * É proibido elfos em venore e anões em carlin

    Mensagens devem ser mostradas para o usuário informando sobre os erros.

``Não esqueça de empacotar a página e enviar ao professor``